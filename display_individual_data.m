close all
clear
clc

load('subject_average_t7f4.mat');
load('subject_average_cpza1a2.mat');
load('individual_data.mat');

%% Grand average EP

% Compute and plot grand averages
cpza1a2_healthy = mean(subject_average_cpza1a2(individual_data.diagnose=="Healthy",:));
cpza1a2_healthy_95ci = 1.96*std(subject_average_cpza1a2(individual_data.diagnose=="Healthy",:));
cpza1a2_pspst2 = mean(subject_average_cpza1a2(individual_data.diagnose=="PSPST2",:));
cpza1a2_pspst2_95ci = 1.96*std(subject_average_cpza1a2(individual_data.diagnose=="PSPST2",:));
t7f4_healthy = mean(subject_average_t7f4(individual_data.diagnose=="Healthy",:));
t7f4_healthy_95ci = 1.96*std(subject_average_t7f4(individual_data.diagnose=="Healthy",:));
t7f4_pspst2 = mean(subject_average_t7f4(individual_data.diagnose=="PSPST2",:));
t7f4_pspst2_95ci = 1.96*std(subject_average_t7f4(individual_data.diagnose=="PSPST2",:));

% Define time axis and fontsize
time = -0.5:1/1000:1-1/1000;
fontsize = 14;

% Plot T7-F4
figure('color','w','position',[0 50 600 550]),hold on;
h1=plot(time,t7f4_healthy,'-','LineWidth',2,'Color','g');
h2=plot(time,t7f4_healthy+t7f4_healthy_95ci,':','LineWidth',2,'Color','g');
plot(time,t7f4_healthy-t7f4_healthy_95ci,':','LineWidth',2,'Color','g');
h3=plot(time,t7f4_pspst2,'-','LineWidth',2,'Color','r');
h4=plot(time,t7f4_pspst2+t7f4_pspst2_95ci,':','LineWidth',2,'Color','r');
plot(time,t7f4_pspst2-t7f4_pspst2_95ci,':','LineWidth',2,'Color','r');
y_lim=get(gca,'ylim');
plot([0,0],[y_lim(1),y_lim(2)],'LineWidth',3,'Color','k')
plot([0.190,0.190],[y_lim(1),y_lim(2)],'LineWidth',1.5,'Color','k')
text(-0.5,y_lim(1),'T7-F4','fontsize',fontsize,'fontweight','bold','verticalalignment','bottom');
text(0.190,y_lim(1),'190 ms','fontsize',fontsize,'verticalalignment','bottom','horizontalalignment','center');
legend([h1 h2 h3 h4],'Grand Average HC','±95% CI','Grand Average PSPS-T2', '±95% CI','Location','SouthWest');
ylabel('Potential (\muV)')
xlabel('Time (s)');
set(gca,'fontsize',fontsize,'ydir','reverse','xgrid','on','xtick',[0:0.2:1]);

% Plot CPz-A1A2
figure('color','w','position',[0 50 600 550]),hold on;
h1=plot(time,cpza1a2_healthy,'-','LineWidth',2,'Color','g');
h2=plot(time,cpza1a2_healthy+cpza1a2_healthy_95ci,':','LineWidth',2,'Color','g');
plot(time,cpza1a2_healthy-cpza1a2_healthy_95ci,':','LineWidth',2,'Color','g');
h3=plot(time,cpza1a2_pspst2,'-','LineWidth',2,'Color','r');
h4=plot(time,cpza1a2_pspst2+cpza1a2_pspst2_95ci,':','LineWidth',2,'Color','r');
plot(time,cpza1a2_pspst2-cpza1a2_pspst2_95ci,':','LineWidth',2,'Color','r');
y_lim=get(gca,'ylim');
plot([0,0],[y_lim(1),y_lim(2)],'LineWidth',3,'Color','k')
plot([0.440,0.440],[y_lim(1),y_lim(2)],'LineWidth',1.5,'Color','k')
text(-0.5,y_lim(1),'CPz-A1A2','fontsize',fontsize,'fontweight','bold','verticalalignment','bottom');
text(0.440,y_lim(1),'440 ms','fontsize',fontsize,'verticalalignment','bottom','horizontalalignment','center');
legend([h1 h2 h3 h4],'Grand Average HC','±95% CI','Grand Average PSPS-T2', '±95% CI','Location','SouthWest');
ylabel('Potential (\muV)')
xlabel('Time (s)');
set(gca,'fontsize',fontsize,'ydir','reverse','xgrid','on','xtick',[0:0.2:1]);


%% Detection rate

% Significance testing: 1) define groups, 2) exclude outliers, if any, 3)
% two-sample t-test.

% Define groups and exclude outliers
hc_dr = individual_data.detection_rate(individual_data.diagnose=="Healthy",:);
hc_dr = hc_dr(~isoutlier(hc_dr,'quartiles')); % exclude outliers at >1.5 IQR
psps_dr = individual_data.detection_rate(individual_data.diagnose=="PSPST2",:);
psps_dr = psps_dr(~isoutlier(psps_dr,'quartiles')); % exclude outliers at >1.5 IQR

% Significance testing
[~,p_dr]=ttest2(hc_dr,psps_dr); % Note that detection thresholds are log-transformed for significance testing

% Make boxplot:
group_index = 5; % index of table column with information about the group
group_labels = {'HC|','PSPS-T2'}; % group labels
column_indices = [7]; % indices of table columns to include in boxplot
column_labels = {{'Detection Rate','','',''}}; % labels of columns in boxplot
column_significance = [p_dr]; % p-values resulting from comparison of both groups for each column
y_label = 'Detection Rate'; % label of y axis
fontsize = 12; % fontsize

% This function creates a boxplot, and was adapted from its original
% function 'create_boxplot()' to also show which patients took medication and
% which not.
create_boxplot_withmed(individual_data,group_index,group_labels,column_indices,column_labels,column_significance,y_label,fontsize);


%% Response time

% Significance testing: 1) define groups, 2) exclude outliers, if any, 3)
% two-sample t-test.

% Define groups and exclude outliers
hc_rt = individual_data.mean_rt(individual_data.diagnose=="Healthy",:);
hc_rt = hc_rt(~isoutlier(hc_rt,'quartiles')); % exclude outliers at >1.5 IQR
psps_rt = individual_data.mean_rt(individual_data.diagnose=="PSPST2",:);
psps_rt = psps_rt(~isoutlier(psps_rt,'quartiles')); % exclude outliers at >1.5 IQR

% Significance testing
[~,p_rt]=ttest2(hc_rt,psps_rt); % Note that detection thresholds are log-transformed for significance testing

% Make boxplot:
group_index = 5; % index of table column with information about the group
group_labels = {'HC|','PSPS-T2'}; % group labels
column_indices = [8]; % indices of table columns to include in boxplot
column_labels = {{'Response Time','','',''}}; % labels of columns in boxplot
column_significance = [p_rt]; % p-values resulting from comparison of both groups for each column
y_label = 'Response Time (s)'; % label of y axis
fontsize = 12; % fontsize

% This function creates a boxplot, and was adapted from its original
% function 'create_boxplot()' to also show which patients took medication and
% which not.
create_boxplot_withmed(individual_data,group_index,group_labels,column_indices,column_labels,column_significance,y_label,fontsize);


%% Detection thresholds

% Significance testing: 1) define groups, 2) exclude outliers, if any, 3)
% two-sample t-test. Process is repeated for each stimulus type.

% Define groups and exclude outliers
hc_sp = individual_data.threshold_sp(individual_data.diagnose=="Healthy",:);
hc_sp = hc_sp(~isoutlier(hc_sp,'quartiles')); % exclude outliers at >1.5 IQR
psps_sp = individual_data.threshold_sp(individual_data.diagnose=="PSPST2",:);
psps_sp = psps_sp(~isoutlier(psps_sp,'quartiles')); % exclude outliers at >1.5 IQR

hc_dp_10 = individual_data.threshold_dp_10(individual_data.diagnose=="Healthy",:);
hc_dp_10 = hc_dp_10(~isoutlier(hc_dp_10,'quartiles')); % exclude outliers at >1.5 IQR
psps_dp_10 = individual_data.threshold_dp_10(individual_data.diagnose=="PSPST2",:);
psps_dp_10 = psps_dp_10(~isoutlier(psps_dp_10,'quartiles')); % exclude outliers at >1.5 IQR

hc_dp_40 = individual_data.threshold_dp_40(individual_data.diagnose=="Healthy",:);
hc_dp_40 = hc_dp_40(~isoutlier(hc_dp_40,'quartiles')); % exclude outliers at >1.5 IQR
psps_dp_40 = individual_data.threshold_dp_40(individual_data.diagnose=="PSPST2",:);
psps_dp_40 = psps_dp_40(~isoutlier(psps_dp_40,'quartiles')); % exclude outliers at >1.5 IQR

% Significance testing
[~,p_sp]=ttest2(log(hc_sp),log(psps_sp)); % Note that detection thresholds are log-transformed for significance testing
[~,p_dp_10]=ttest2(log(hc_dp_10),log(psps_dp_10)); 
[~,p_dp_40]=ttest2(log(hc_dp_40),log(psps_dp_40)); 

% Make boxplot:
group_index = 5; % index of table column with information about the group
group_labels = {'HC|','PSPS-T2'}; % group labels
column_indices = [15,16,17]; % indices of table columns to include in boxplot
column_labels = {{'NDT Single-Pulse','','',''},{'NDT Double-Pulse','10ms IPI','',''},{'NDT Double-Pulse','40ms IPI','',''}}; % labels of columns in boxplot
column_significance = [p_sp,p_dp_10,p_dp_40]; % p-values resulting from comparison of both groups for each column
y_label = 'NDT (mA)'; % label of y axis
fontsize = 12; % fontsize

% This function creates a boxplot, and was adapted from its original
% function 'create_boxplot()' to also show which patients took medication and
% which not.
create_boxplot_withmed(individual_data,group_index,group_labels,column_indices,column_labels,column_significance,y_label,fontsize);


%% Detection Thresholds: Stimulus effects

% Significance testing: 1) define groups, 2) exclude outliers, if any, 3)
% two-sample t-test. Process is repeated for each coefficient.

% Define groups and exclude outliers
hc_p1 = individual_data.glm_p1(individual_data.diagnose=="Healthy",:);
hc_p1 = hc_p1(~isoutlier(hc_p1,'quartiles')); % exclude outliers at >1.5 IQR
psps_p1 = individual_data.glm_p1(individual_data.diagnose=="PSPST2",:);
psps_p1 = psps_p1(~isoutlier(psps_p1,'quartiles')); % exclude outliers at >1.5 IQR

hc_p2_10 = individual_data.glm_p2_10(individual_data.diagnose=="Healthy",:);
hc_p2_10 = hc_p2_10(~isoutlier(hc_p2_10,'quartiles')); % exclude outliers at >1.5 IQR
psps_p2_10 = individual_data.glm_p2_10(individual_data.diagnose=="PSPST2",:);
psps_p2_10 = psps_p2_10(~isoutlier(psps_p2_10,'quartiles')); % exclude outliers at >1.5 IQR

hc_p2_40 = individual_data.glm_p2_40(individual_data.diagnose=="Healthy",:);
hc_p2_40 = hc_p2_40(~isoutlier(hc_p2_40,'quartiles')); % exclude outliers at >1.5 IQR
psps_p2_40 = individual_data.glm_p2_40(individual_data.diagnose=="PSPST2",:);
psps_p2_40 = psps_p2_40(~isoutlier(psps_p2_40,'quartiles')); % exclude outliers at >1.5 IQR

% Determine lowest negative value of coefficients, to be subtracted
% before log-transformation (to prevent negative values in log)
c = min([hc_p1;psps_p1;hc_p2_10;psps_p2_10;hc_p2_40;psps_p2_40]);

% Significance testing
[~,p_p1]=ttest2(log(hc_p1+c),log(psps_p1+c)); % Note that psychometric slopes are log-transformed for significance testing
[~,p_p2_10]=ttest2(log(hc_p2_10+c),log(psps_p2_10+c));
[~,p_p2_40]=ttest2(log(hc_p2_40+c),log(psps_p2_40+c));

% Make boxplot:
group_index = 5; % index of table column with information about the group
group_labels = {'HC|','PSPS-T2'}; % group labels
column_indices = [18,19,20]; % indices of table columns to include in boxplot
column_labels = {{'PU1 on P_{d}','','',''},{'PU2_{10} on P_{d}','','',''},{'PU2_{40} on P_{d}','','',''}}; % labels of columns in boxplot
column_significance = [p_p1,p_p2_10,p_p2_40]; % p-values resulting from comparison of both groups for each column
y_label = 'Coefficient (mA^{-1})'; % label of y axis
fontsize = 12; % fontsize

% This function creates a boxplot, and was adapted from its original
% function 'create_boxplot()' to also show which patients took medication and
% which not.
create_boxplot_withmed(individual_data,group_index,group_labels,column_indices,column_labels,column_significance,y_label,fontsize);


%% Evoked Potentials N1

% Significance testing: 1) define groups, 2) exclude outliers, if any, 3)
% two-sample t-test. Process is repeated for each stimulus type.

% Define groups and exclude outliers
hc_sp = individual_data.N1_mean_sp(individual_data.diagnose=="Healthy",:);
hc_sp = hc_sp(~isoutlier(hc_sp,'quartiles')); % exclude outliers at >1.5 IQR
psps_sp = individual_data.N1_mean_sp(individual_data.diagnose=="PSPST2",:);
psps_sp = psps_sp(~isoutlier(psps_sp,'quartiles')); % exclude outliers at >1.5 IQR

hc_dp_10 = individual_data.N1_mean_dp_10(individual_data.diagnose=="Healthy",:);
hc_dp_10 = hc_dp_10(~isoutlier(hc_dp_10,'quartiles')); % exclude outliers at >1.5 IQR
psps_dp_10 = individual_data.N1_mean_dp_10(individual_data.diagnose=="PSPST2",:);
psps_dp_10 = psps_dp_10(~isoutlier(psps_dp_10,'quartiles')); % exclude outliers at >1.5 IQR

hc_dp_40 = individual_data.N1_mean_dp_40(individual_data.diagnose=="Healthy",:);
hc_dp_40 = hc_dp_40(~isoutlier(hc_dp_40,'quartiles')); % exclude outliers at >1.5 IQR
psps_dp_40 = individual_data.N1_mean_dp_40(individual_data.diagnose=="PSPST2",:);
psps_dp_40 = psps_dp_40(~isoutlier(psps_dp_40,'quartiles')); % exclude outliers at >1.5 IQR

% Significance testing
[~,p_sp]=ttest2(hc_sp,psps_sp); % Note that detection thresholds are log-transformed for significance testing
[~,p_dp_10]=ttest2(hc_dp_10,psps_dp_10); 
[~,p_dp_40]=ttest2(hc_dp_40,psps_dp_40); 

% Make boxplot:
group_index = 5; % index of table column with information about the group
group_labels = {'HC|','PSPS-T2'}; % group labels
column_indices = [22,23,24]; % indices of table columns to include in boxplot
column_labels = {{'N190 Single-Pulse','','',''},{'N190 Double-Pulse','10ms IPI','',''},{'N190 Double-Pulse','40ms IPI','',''}}; % labels of columns in boxplot
column_significance = [p_sp,p_dp_10,p_dp_40]; % p-values resulting from comparison of both groups for each column
y_label = 'Potential (\muV)'; % label of y axis
fontsize = 12; % fontsize

% This function creates a boxplot, and was adapted from its original
% function 'create_boxplot()' to also show which patients took medication and
% which not.
create_boxplot_withmed(individual_data,group_index,group_labels,column_indices,column_labels,column_significance,y_label,fontsize);


%% Evoked Potentials P2

% Significance testing: 1) define groups, 2) exclude outliers, if any, 3)
% two-sample t-test. Process is repeated for each stimulus type.

% Define groups and exclude outliers
hc_sp = individual_data.P2_mean_sp(individual_data.diagnose=="Healthy",:);
hc_sp = hc_sp(~isoutlier(hc_sp,'quartiles')); % exclude outliers at >1.5 IQR
psps_sp = individual_data.P2_mean_sp(individual_data.diagnose=="PSPST2",:);
psps_sp = psps_sp(~isoutlier(psps_sp,'quartiles')); % exclude outliers at >1.5 IQR

hc_dp_10 = individual_data.P2_mean_dp_10(individual_data.diagnose=="Healthy",:);
hc_dp_10 = hc_dp_10(~isoutlier(hc_dp_10,'quartiles')); % exclude outliers at >1.5 IQR
psps_dp_10 = individual_data.P2_mean_dp_10(individual_data.diagnose=="PSPST2",:);
psps_dp_10 = psps_dp_10(~isoutlier(psps_dp_10,'quartiles')); % exclude outliers at >1.5 IQR

hc_dp_40 = individual_data.P2_mean_dp_40(individual_data.diagnose=="Healthy",:);
hc_dp_40 = hc_dp_40(~isoutlier(hc_dp_40,'quartiles')); % exclude outliers at >1.5 IQR
psps_dp_40 = individual_data.P2_mean_dp_40(individual_data.diagnose=="PSPST2",:);
psps_dp_40 = psps_dp_40(~isoutlier(psps_dp_40,'quartiles')); % exclude outliers at >1.5 IQR

% Significance testing
[~,p_sp]=ttest2(hc_sp,psps_sp); % Note that detection thresholds are log-transformed for significance testing
[~,p_dp_10]=ttest2(hc_dp_10,psps_dp_10); 
[~,p_dp_40]=ttest2(hc_dp_40,psps_dp_40); 

% Make boxplot:
group_index = 5; % index of table column with information about the group
group_labels = {'HC|','PSPS-T2'}; % group labels
column_indices = [33,34,35]; % indices of table columns to include in boxplot
column_labels = {{'P440 Single-Pulse','','',''},{'P440 Double-Pulse','10ms IPI','',''},{'P440 Double-Pulse','40ms IPI','',''}}; % labels of columns in boxplot
column_significance = [p_sp,p_dp_10,p_dp_40]; % p-values resulting from comparison of both groups for each column
y_label = 'Potential (\muV)'; % label of y axis
fontsize = 12; % fontsize

% This function creates a boxplot, and was adapted from its original
% function 'create_boxplot()' to also show which patients took medication and
% which not.
create_boxplot_withmed(individual_data,group_index,group_labels,column_indices,column_labels,column_significance,y_label,fontsize);


%% Evoked Potentials N1: Stimulus effects

% Significance testing: 1) define groups, 2) exclude outliers, if any, 3)
% two-sample t-test. Process is repeated for each coefficient.

% Define groups and exclude outliers
hc_p1 = individual_data.N1_lm_p1(individual_data.diagnose=="Healthy",:);
hc_p1 = hc_p1(~isoutlier(hc_p1,'quartiles')); % exclude outliers at >1.5 IQR
psps_p1 = individual_data.N1_lm_p1(individual_data.diagnose=="PSPST2",:);
psps_p1 = psps_p1(~isoutlier(psps_p1,'quartiles')); % exclude outliers at >1.5 IQR

hc_p2_10 = individual_data.N1_lm_p2_10(individual_data.diagnose=="Healthy",:);
hc_p2_10 = hc_p2_10(~isoutlier(hc_p2_10,'quartiles')); % exclude outliers at >1.5 IQR
psps_p2_10 = individual_data.N1_lm_p2_10(individual_data.diagnose=="PSPST2",:);
psps_p2_10 = psps_p2_10(~isoutlier(psps_p2_10,'quartiles')); % exclude outliers at >1.5 IQR

hc_p2_40 = individual_data.N1_lm_p2_40(individual_data.diagnose=="Healthy",:);
hc_p2_40 = hc_p2_40(~isoutlier(hc_p2_40,'quartiles')); % exclude outliers at >1.5 IQR
psps_p2_40 = individual_data.N1_lm_p2_40(individual_data.diagnose=="PSPST2",:);
psps_p2_40 = psps_p2_40(~isoutlier(psps_p2_40,'quartiles')); % exclude outliers at >1.5 IQR

% Significance testing
[~,p_sp]=ttest2(hc_p1,psps_p1); % Note that detection thresholds are log-transformed for significance testing
[~,p_dp_10]=ttest2(hc_p2_10,psps_p2_10); 
[~,p_dp_40]=ttest2(hc_p2_40,psps_p2_40); 

% Make boxplot:
group_index = 5; % index of table column with information about the group
group_labels = {'HC|','PSPS-T2'}; % group labels
column_indices = [26,27,28]; % indices of table columns to include in boxplot
column_labels = {{'PU1 on N190','','',''},{'PU2_{10} on N190','','',''},{'PU2_{40} on N190','','',''}}; % labels of columns in boxplot
column_significance = [p_sp,p_dp_10,p_dp_40]; % p-values resulting from comparison of both groups for each column
y_label = 'Coefficient (\muV/mA)'; % label of y axis
fontsize = 12; % fontsize

% This function creates a boxplot, and was adapted from its original
% function 'create_boxplot()' to also show which patients took medication and
% which not.
create_boxplot_withmed(individual_data,group_index,group_labels,column_indices,column_labels,column_significance,y_label,fontsize);


%% Evoked Potentials P2: Stimulus effects

% Significance testing: 1) define groups, 2) exclude outliers, if any, 3)
% two-sample t-test. Process is repeated for each coefficient.

% Define groups and exclude outliers
hc_p1 = individual_data.P2_lm_p1(individual_data.diagnose=="Healthy",:);
hc_p1 = hc_p1(~isoutlier(hc_p1,'quartiles')); % exclude outliers at >1.5 IQR
psps_p1 = individual_data.P2_lm_p1(individual_data.diagnose=="PSPST2",:);
psps_p1 = psps_p1(~isoutlier(psps_p1,'quartiles')); % exclude outliers at >1.5 IQR

hc_p2_10 = individual_data.P2_lm_p2_10(individual_data.diagnose=="Healthy",:);
hc_p2_10 = hc_p2_10(~isoutlier(hc_p2_10,'quartiles')); % exclude outliers at >1.5 IQR
psps_p2_10 = individual_data.P2_lm_p2_10(individual_data.diagnose=="PSPST2",:);
psps_p2_10 = psps_p2_10(~isoutlier(psps_p2_10,'quartiles')); % exclude outliers at >1.5 IQR

hc_p2_40 = individual_data.P2_lm_p2_40(individual_data.diagnose=="Healthy",:);
hc_p2_40 = hc_p2_40(~isoutlier(hc_p2_40,'quartiles')); % exclude outliers at >1.5 IQR
psps_p2_40 = individual_data.P2_lm_p2_40(individual_data.diagnose=="PSPST2",:);
psps_p2_40 = psps_p2_40(~isoutlier(psps_p2_40,'quartiles')); % exclude outliers at >1.5 IQR

% Significance testing
[~,p_sp]=ttest2(hc_p1,psps_p1); % Note that detection thresholds are log-transformed for significance testing
[~,p_dp_10]=ttest2(hc_p2_10,psps_p2_10); 
[~,p_dp_40]=ttest2(hc_p2_40,psps_p2_40); 

% Make boxplot:
group_index = 5; % index of table column with information about the group
group_labels = {'HC|','PSPS-T2'}; % group labels
column_indices = [37,38,39]; % indices of table columns to include in boxplot
column_labels = {{'PU1 on P440','','',''},{'PU2_{10} on P440','','',''},{'PU2_{40} on P440','','',''}}; % labels of columns in boxplot
column_significance = [p_sp,p_dp_10,p_dp_40]; % p-values resulting from comparison of both groups for each column
y_label = 'Coefficient (\muV/mA)'; % label of y axis
fontsize = 12; % fontsize

% This function creates a boxplot, and was adapted from its original
% function 'create_boxplot()' to also show which patients took medication and
% which not.
create_boxplot_withmed(individual_data,group_index,group_labels,column_indices,column_labels,column_significance,y_label,fontsize);

%% Save all figures
save_open_figures('figures','fig');
