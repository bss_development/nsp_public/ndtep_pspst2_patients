function create_boxplot_withmed(data,group_index,group_labels,column_indices,column_labels,column_significance,y_label,fontsize)
    grps = unique(data{:,group_index});
    ngrps = numel(grps);
    ncols = numel(column_indices);

    figure('Units', 'Normalized','Color','w', 'OuterPosition', [0.2 0.2 0.2+0.08*ngrps*ncols 0.6])
    hold on
    
    for i=1:ncols
        maxw_mem = 0;
        for j=1:ngrps
            xpos = i-(floor(ngrps/2)-j+1)*0.3;
            grpinds = find(data{:,group_index}==grps(j));
            ydat = data{grpinds,column_indices(i)};
            med = data.medication(data{:,group_index}==grps(j));

            if j==1
                color1 = [0 1 0];
                color2 = [0 1 0];
            else
                color1 = [1 0 0];
                color2 = [0 0 1];
            end

            q = quantile(ydat,[0.25,0.50,0.75]);
            
            out_inds = find(isoutlier(ydat,'quartiles'));%(ydat>q(3)+outlier_limit*iqr | ydat<q(1)-outlier_limit*iqr);
            in_inds = find(~isoutlier(ydat,'quartiles'));%(ydat<q(3)+outlier_limit*iqr & ydat>q(1)-outlier_limit*iqr);
            
            outliers = ydat(out_inds);
            inliers = ydat(in_inds);
            inliers1 = ydat(~isoutlier(ydat,'quartiles') & med == "No");
            inliers2 = ydat(~isoutlier(ydat,'quartiles') & med == "Yes");
            
            maxw = max(inliers);
            if maxw>maxw_mem
                maxw_mem = maxw;
            end
            
            minw = min(inliers);
            patch([xpos-0.09 xpos-0.09 xpos+0.09 xpos+0.09],[q(1) q(3) q(3) q(1)],[0.7 0.7 0.7],'edgecolor', 'none');
            plot([xpos-0.09 xpos+0.09],[minw minw],'Color',[0 0 0],'LineWidth',1);
            plot([xpos-0.09 xpos+0.09],[maxw maxw],'Color',[0 0 0],'LineWidth',1);
            plot([xpos xpos],[minw maxw],'Color',[0 0 0],'LineWidth',1);
            plot([xpos-0.09 xpos+0.09],[q(2) q(2)],'Color',[0 0 0],'LineWidth',3);
            h2(j)=scatter(xpos*ones(size(inliers2)),inliers2,30,'MarkerFaceColor',color2,'MarkerEdgeColor',color2);
            h1(j)=scatter(xpos*ones(size(inliers1)),inliers1,30,'MarkerFaceColor',color1,'MarkerEdgeColor',color1);

            xposlist(i,j) = xpos;
            outlist(i,j) = numel(outliers);
            colorlist(i,j,:) = color1;
            
            outlier_indices{i,j} = grpinds(out_inds);
        end
    end

    y_lim = get(gca,'ylim');
    h = y_lim(2)-y_lim(1);
    ylim([y_lim(1) y_lim(2)+0.2*h]);
    y_lim = get(gca,'ylim');
    h = y_lim(2)-y_lim(1);

    xlabs = {};
    xtks = [];
    tab1 = table();
    tab2 = table();
    
    for i=1:numel(column_labels)
        text(i,y_lim(2)+h/16,column_labels{i},'HorizontalAlignment','center','fontsize',fontsize);
        
        for j=1:numel(group_labels)
            h3 = scatter(xposlist(i,j),y_lim(2),30,'MarkerFaceColor',[1 1 1],'MarkerEdgeColor','k');
            text(xposlist(i,j)-0.15,y_lim(2),num2str(outlist(i,j)),'fontsize',fontsize);
            xlabs = {xlabs{:},group_labels{j}};
            xtks = [xtks,xposlist(i,j)];
        end
        
        if column_significance(i)<0.001
            plot([xposlist(i,1) xposlist(i,end)],[y_lim(2)-0.1*h y_lim(2)-0.1*h],'Color',[0 0 0],'LineWidth',2)
            plot([xposlist(i,1) xposlist(i,1)],[y_lim(2)-0.15*h y_lim(2)-0.05*h],'Color',[0 0 0],'LineWidth',2)
            plot([xposlist(i,end) xposlist(i,end)],[y_lim(2)-0.15*h y_lim(2)-0.05*h],'Color',[0 0 0],'LineWidth',2)
            text((xposlist(i,1)+xposlist(i,end))/2,y_lim(2)-0.075*h,'***','HorizontalAlignment','center','fontsize',fontsize);
        elseif column_significance(i)<0.01
            plot([xposlist(i,1) xposlist(i,end)],[y_lim(2)-0.1*h y_lim(2)-0.1*h],'Color',[0 0 0],'LineWidth',2)
            plot([xposlist(i,1) xposlist(i,1)],[y_lim(2)-0.15*h y_lim(2)-0.05*h],'Color',[0 0 0],'LineWidth',2)
            plot([xposlist(i,end) xposlist(i,end)],[y_lim(2)-0.15*h y_lim(2)-0.05*h],'Color',[0 0 0],'LineWidth',2)
            text((xposlist(i,1)+xposlist(i,end))/2,y_lim(2)-0.075*h,'**','HorizontalAlignment','center','fontsize',fontsize);
        elseif column_significance(i)<0.05
            plot([xposlist(i,1) xposlist(i,end)],[y_lim(2)-0.1*h y_lim(2)-0.1*h],'Color',[0 0 0],'LineWidth',2)
            plot([xposlist(i,1) xposlist(i,1)],[y_lim(2)-0.15*h y_lim(2)-0.05*h],'Color',[0 0 0],'LineWidth',2)
            plot([xposlist(i,end) xposlist(i,end)],[y_lim(2)-0.15*h y_lim(2)-0.05*h],'Color',[0 0 0],'LineWidth',2)
            text((xposlist(i,1)+xposlist(i,end))/2,y_lim(2)-0.075*h,'*','HorizontalAlignment','center','fontsize',fontsize);
        end
    end
    
    xticks(xtks);
    xticklabels(xlabs);
    xlim([0.5 ncols+0.5]);
    ylabel(y_label);
    
    legend([h1(1) h1(2) h2(2) h3],'HC','PSPS-T2 (no med.)','PSPS-T2 (med.)','Outliers (>1.5 IQR)','Location','EastOutside')

    set(gca,'fontsize',fontsize);
    
    scale = 0.89;
    pos = get(gca, 'Position');
    pos(3) = scale*pos(3);
    pos(4) = scale*pos(4);
    set(gca, 'Position', pos)
end

