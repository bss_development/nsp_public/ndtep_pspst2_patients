function save_open_figures(folder,name)
    
    % Get a list of all figure handles.
    h = get(0,'children');
    
    % For every figure handle:
    for i=1:length(h)
        figure(h(i));

        path = [folder '\' name '_' num2str(i)];

        print(gcf,path,'-dpng','-r300');
        savefig(path)     
    end
end

