close all
clear
clc

folder_ndt = 'NDT'; % folder with detection data, for computation of nociceptive detection thresholds (NDTs)
folder_t7f4 = 'EEG_T7F4'; % folder with EEG data from the T7-F4 derivation
folder_cpza1a2 = 'EEG_CPzA1A2'; % folder with EEG data from the CPz-A1A2 derivation

f_sample = 1000; % sample frequency
time_interval = [-0.5,1.0]; % time interval of EEG epochs with respect to stimulus
time_N1 = 0.190; % time of N1 peak in the grand average, in seconds
time_P2 = 0.440; % time of P2 peak in the grand average, in seconds

% compute indices of N1 and P2
N1_index = ceil((time_N1-time_interval(1))*f_sample);
P2_index = ceil((time_P2-time_interval(1))*f_sample);

ndt_files = dir(folder_ndt);
ndt_files = {ndt_files(3:end).name}; % note that first two filenames refer to empty files, '.' and '..', and are removed

t7f4_files = dir(folder_t7f4);
t7f4_files = {t7f4_files(3:end).name}; 

cpza1a2_files = dir(folder_cpza1a2);
cpza1a2_files = {cpza1a2_files(3:end).name}; 

% initialize table
individual_data = table();

% for each recording
for i=1:numel(ndt_files)
    % load detection data
    load(fullfile(folder_ndt,ndt_files{i}));
    
    % write subject characteristics to table
    individual_data(i,threshold_data.Properties.VariableNames(9:14)) = threshold_data(1,threshold_data.Properties.VariableNames(9:14));
    
    % compute detection rate, and mean & standard deviation of response time
    individual_data.detection_rate(i) = sum(threshold_data.detection)/numel(threshold_data.detection);
    individual_data.mean_rt(i) = mean(threshold_data.response_time,'omitnan');
    individual_data.sd_rt(i) = std(threshold_data.response_time,'omitnan');
    
    % standardize trial number and fit GLM
    [threshold_data.trial_nr_standardized,~,sd_trl] = standardize(threshold_data.trial_nr);    
    glm = fitglm(threshold_data,'detection~1+p1+p2_10+p2_40+trial_nr_standardized','Distribution','Binomial');
    
    % X=[threshold_data.p1,threshold_data.p2_10,threshold_data.p2_40,threshold_data.trial_nr_standardized];
    % vif_glm(i,:) = vif(X);
    
    % write GLM coefficients to table
    individual_data.glm_int(i) = glm.Coefficients.Estimate(1);
    individual_data.glm_p1(i) = glm.Coefficients.Estimate(2);
    individual_data.glm_p2_10(i) = glm.Coefficients.Estimate(3);
    individual_data.glm_p2_40(i) = glm.Coefficients.Estimate(4);
    individual_data.glm_trl(i) = glm.Coefficients.Estimate(5)/sd_trl; % note that the effect is rescaled to the unit trial^-1

    % compute and write detection thresholds to table
    individual_data.threshold_sp(i) = -individual_data.glm_int(i)./(individual_data.glm_p1(i));
    individual_data.threshold_dp_10(i) = -individual_data.glm_int(i)./(individual_data.glm_p1(i)+individual_data.glm_p2_10(i));
    individual_data.threshold_dp_40(i) = -individual_data.glm_int(i)./(individual_data.glm_p1(i)+individual_data.glm_p2_40(i));
    
    % compute and write psychometric slopes to table
    individual_data.slope_sp(i) = individual_data.glm_p1(i);
    individual_data.slope_dp_10(i) = individual_data.glm_p1(i)+individual_data.glm_p2_10(i);
    individual_data.slope_dp_40(i) = individual_data.glm_p1(i)+individual_data.glm_p2_40(i);
    
    % load eeg at T7-F4
    load(fullfile(folder_t7f4,t7f4_files{i}));
    eeg = ep_data.eeg;
    
    % compute subject average
    subject_average_t7f4(i,:) = mean(eeg,1);
    
    % compute N1 mean for each stimulus type and write to table
    individual_data.N1_mean(i) = mean(eeg(:,N1_index));
    individual_data.N1_mean_sp(i) = mean(eeg(ep_data.type==1,N1_index));
    individual_data.N1_mean_dp_10(i) = mean(eeg(ep_data.type==2,N1_index));
    individual_data.N1_mean_dp_40(i) = mean(eeg(ep_data.type==3,N1_index));
    
    % prepare eeg data for LM
    ep_data.eeg = ep_data.eeg(:,N1_index); % we take only eeg at index of the N1
    [ep_data.trial_nr_standardized,~,sd_trl] = standardize(ep_data.trial_nr); % we standardize trial number (else the range of trial number is very different from the other parameters)
    ep_data.detection = categorical(ep_data.detection,[0,1],'Ordinal',true); % we define detection as a categorical

    lm = fitlm(ep_data,'eeg~1+p1+p2_10+p2_40+detection*trial_nr_standardized','DummyVarCoding','reference');
    beta = lm.Coefficients.Estimate;
    betanames = lm.Coefficients.Properties.RowNames;

    % write GLM coefficients to table
    individual_data.N1_lm_int(i) = lm.Coefficients.Estimate(1);
    individual_data.N1_lm_p1(i) = lm.Coefficients.Estimate(2);
    individual_data.N1_lm_p2_10(i) = lm.Coefficients.Estimate(3);
    individual_data.N1_lm_p2_40(i) = lm.Coefficients.Estimate(4);
    individual_data.N1_lm_d(i) = lm.Coefficients.Estimate(5);
    individual_data.N1_lm_trl(i) = lm.Coefficients.Estimate(6)/sd_trl; % note that the effect is rescaled to the unit trial^-1
    individual_data.N1_lm_d_trl(i) = lm.Coefficients.Estimate(7)/sd_trl; % note that the effect is rescaled to the unit trial^-1
    
    % load eeg at CPz-A1A2
    load(fullfile(folder_cpza1a2,cpza1a2_files{i}));
    eeg = ep_data.eeg;
    
    % compute subject average
    subject_average_cpza1a2(i,:) = mean(eeg,1);
    
    % compute P2 mean for each stimulus type and write to table
    individual_data.P2_mean(i) = mean(eeg(:,P2_index));
    individual_data.P2_mean_sp(i) = mean(eeg(ep_data.type==1,P2_index));
    individual_data.P2_mean_dp_10(i) = mean(eeg(ep_data.type==2,P2_index));
    individual_data.P2_mean_dp_40(i) = mean(eeg(ep_data.type==3,P2_index));
    
    % prepare eeg data for LM
    ep_data.eeg = ep_data.eeg(:,P2_index); % we take only eeg at index of the P2
    [ep_data.trial_nr_standardized,~,sd_trl] = standardize(ep_data.trial_nr); % we standardize trial number (else the range of trial number is very different from the other parameters)
    ep_data.detection = categorical(ep_data.detection,[0,1],'Ordinal',true); % we define detection as a categorical

    lm = fitlm(ep_data,'eeg~1+p1+p2_10+p2_40+detection*trial_nr_standardized','DummyVarCoding','reference');
    beta = lm.Coefficients.Estimate;
    betanames = lm.Coefficients.Properties.RowNames;
    
    % X=[threshold_data.p1,threshold_data.p2_10,threshold_data.p2_40,threshold_data.trial_nr_standardized,threshold_data.detection];
    % vif_lm(i,:) = vif(X);

    % write GLM coefficients to table
    individual_data.P2_lm_int(i) = lm.Coefficients.Estimate(1);
    individual_data.P2_lm_p1(i) = lm.Coefficients.Estimate(2);
    individual_data.P2_lm_p2_10(i) = lm.Coefficients.Estimate(3);
    individual_data.P2_lm_p2_40(i) = lm.Coefficients.Estimate(4);
    individual_data.P2_lm_d(i) = lm.Coefficients.Estimate(5);
    individual_data.P2_lm_trl(i) = lm.Coefficients.Estimate(6)/sd_trl; % note that the effect is rescaled to the unit trial^-1
    individual_data.P2_lm_d_trl(i) = lm.Coefficients.Estimate(7)/sd_trl; % note that the effect is rescaled to the unit trial^-1
end

save('subject_average_t7f4.mat','subject_average_t7f4');
save('subject_average_cpza1a2.mat','subject_average_cpza1a2');
save('individual_data.mat','individual_data');

